import React from 'react';
import {BrowserRouter as Router, Route, Switch,} from 'react-router-dom';

import clsx from 'clsx';
import {makeStyles} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import MenuIcon from '@material-ui/icons/Menu';
import {Lecture2Examples} from './modules/lecture-2';
import MaterialUICollapseMenu from 'material-ui-collapse-menu'
import {Lecture3Examples} from './modules/lecture-3';
import {Lecture1Examples} from './modules/lecture-1';
import {Lecture4Examples} from './modules/lecture-4';
import {Lecture5Examples} from './modules/lecture-5/app';
import {Lecture6Examples} from './modules/lecture-6';
import {Lecture7Examples} from './modules/lecture-7';
import {Provider} from 'react-redux';
import {store} from './modules/lecture-7/store/store';
import {Lecture9Examples} from './modules/lecture-9';

const drawerWidth = 240;

const useStyles = makeStyles((theme: any) => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    }
}));


const menuItems = [
    {
        id: 1,
        title: '',
        items: [
            {
                id: 'l1',
                name: 'Lecture 1',
                link: '/lecture-1/user-cards',
            }
        ]
    },
    {
        id: 2,
        title: '',
        items: [
            {
                id: 3,
                icon: '',
                name: 'Lecture 2',
                subitems: [
                    {id: 'class', name: 'Class', link: '/lecture-2/class-example'},
                    {id: 'functional', name: 'Functional', link: '/lecture-2/functional-example'},
                    {id: 'props', name: 'Props', link: '/lecture-2/props-example'},
                    {id: 'state', name: 'State', link: '/lecture-2/state-example'},
                    {id: 'mutable State', name: 'Mutable State', link: '/lecture-2/pure-component-example'},
                    {id: 'parent -> Child', name: 'Parent -> Child', link: '/lecture-2/parent-child-example'},
                    {id: 'event handling', name: 'Event handling', link: '/lecture-2/input-event-handling-example'}
                ]
            }
        ]
    },
    {
        id: 3,
        title: '',
        items: [
            {
                id: 'l3-menu',
                icon: '',
                name: 'Lecture 3',
                subitems: [
                    {id: 'context', name: 'Context', link: '/lecture-3/context-example'},
                    {id: 'context-class', name: 'Context (class)', link: '/lecture-3/context-example-class'},
                    {id: 'refs', name: 'Refs', link: '/lecture-3/refs-example'},
                    {id: 'refs-fn', name: 'Refs (fn)', link: '/lecture-3/refs-hook'},
                ]
            }
        ]
    },
    {
        id: 4,
        title: '',
        items: [
            {
                id: 'l4-menu',
                icon: '',
                name: 'Lecture 4',
                subitems: [
                    {id: 'toggle', name: 'Toggle', link: '/lecture-4/toggle-example'},
                    {id: 'context', name: 'Countdown', link: '/lecture-4/countdown-example'},
                    {id: 'context-class', name: 'Countdown (hooks)', link: '/lecture-4/countdown-hooks-example'},
                    {id: 'useCallback', name: 'useCallback', link: '/lecture-4/use-callback-example'},
                    {id: 'useMemo', name: 'useMemo', link: '/lecture-4/heavy-calc-example'},
                    {id: 'usePagination', name: 'usePagination', link: '/lecture-4/use-pagination-example'},
                    {id: 'usePagination2', name: 'usePagination ex 2', link: '/lecture-1/user-cards'},
                    {id: 'useLogger', name: 'useLogger', link: '/lecture-4/use-logger-example'},
                ]
            }
        ]
    },
    {
        id: 5,
        title: '',
        items: [
            {
                id: 'l5-menu',
                icon: '',
                name: 'Lecture 5',
                link: '/lecture-5'
            }
        ]
    },
    {
        id: 6,
        title: '',
        items: [
            {
                id: 'l6-menu',
                icon: '',
                name: 'Lecture 6',
                link: '/lecture-6',
                subitems: [
                    {id: 'carousel', name: 'Carousel', link: '/lecture-6/carousel'},
                    {id: 'useLogger', name: 'useLogger', link: '/lecture-6/use-logger'},
                    {id: 'useFilter', name: 'useFilter', link: '/lecture-6/use-filter'},
                    {id: 'with-error', name: 'With Error', link: '/lecture-6/with-error-fallback'},
                    {id: 'hoc', name: 'HOC', link: '/lecture-6/hoc-usage'},
                    {id: 'users', name: 'Users List', link: '/lecture-6/users/list'},
                    {id: 'users-grid', name: 'Users Grid', link: '/lecture-6/users/grid'},
                    {id: 'users-grid-paginated', name: 'Users Grid Paginated', link: '/lecture-6/users/grid-paginated'},
                    {id: 'df-octokit', name: 'Data fetch section', link: '/lecture-6/df-octokit'},
                ]
            }
        ]
    },
    {
        id: 7,
        title: '',
        items: [
            {
                id: 'l7-menu',
                icon: '',
                name: 'Lecture 7',
                link: '/lecture-7',
                subitems: [
                    {id: 'issues', name: 'Github App', link: '/lecture-7'},
                ]
            }
        ]
    },
    {
        id: 9,
        title: '',
        items: [
            {
                id: 'l9-menu',
                icon: '',
                name: 'Lecture 9',
                link: '/lecture-9',
                subitems: [
                    {id: 'issues', name: 'Issues', link: '/lecture-9'},
                ]
            }
        ]
    },
]

export default function App() {

    const [open, setOpen] = React.useState(true);
    const classes = useStyles();
    const handleDrawerOpen = () => {
        setOpen(true);
    };


    return (
        <Provider store={store}>
            <Router>
                <div className={classes.root}>
                    <CssBaseline/>
                    <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
                        <Toolbar className={classes.toolbar}>
                            <IconButton
                                edge="start"
                                color="inherit"
                                onClick={handleDrawerOpen}
                                className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
                            >
                                <MenuIcon/>
                            </IconButton>
                            <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                                SPDU 2020
                            </Typography>
                        </Toolbar>
                    </AppBar>
                    <Drawer
                        variant="permanent"
                        classes={{
                            paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
                        }}
                        open={open}
                    >
                        <MaterialUICollapseMenu items={menuItems}/>
                    </Drawer>
                    <main className={classes.content}>
                        <div className={classes.appBarSpacer}/>
                        <Container maxWidth="lg" className={classes.container}>
                            <div>
                                <Switch>
                                    <Route path="/" component={() => {
                                        return (
                                            <iframe
                                                src="https://spd-ukraine.com/university/"
                                                width="100%"
                                                height="3000px"
                                                frameBorder={0}
                                            />
                                        )
                                    }} exact/>
                                    <Route path="/lecture-1" component={Lecture1Examples}/>
                                    <Route path="/lecture-2" component={Lecture2Examples}/>
                                    <Route path="/lecture-3" component={Lecture3Examples}/>
                                    <Route path="/lecture-4" component={Lecture4Examples}/>
                                    <Route path="/lecture-5" component={Lecture5Examples}/>
                                    <Route path="/lecture-6" component={Lecture6Examples}/>
                                    <Route path="/lecture-7" component={Lecture7Examples}/>
                                    <Route path="/lecture-9" component={Lecture9Examples}/>
                                </Switch>
                            </div>
                        </Container>
                    </main>
                </div>
            </Router>
        </Provider>
    );
}