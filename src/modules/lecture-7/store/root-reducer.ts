import {combineReducers} from 'redux';
import {issues} from '../modules/issues/services/reducers';
import {comments} from '../modules/issues/comments/services/reducers';
import {repoSearch} from '../modules/repos-search/services/repo-search-slice';
import {repoDetails} from '../modules/repo-details/services/repo-details-slice';

export const rootReducer = combineReducers({
    issues,
    comments,
    reposSearch: repoSearch.reducer,
    repoDetails: repoDetails.reducer
})