import { configureStore } from '@reduxjs/toolkit'
import {rootReducer} from './root-reducer';
import {handleErrors} from './handle-errors';



export const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware => getDefaultMiddleware().concat(handleErrors)
});