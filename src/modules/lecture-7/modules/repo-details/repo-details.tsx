import {Box, Card, CardContent, CardHeader} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import {NavLink, useRouteMatch} from "react-router-dom";

import {useParams} from 'react-router-dom';
import {useEffect} from 'react';
import {loadRepoInfo, repoSelectors} from './services/repo-details-slice';
import {makeOwnerRepoId} from '../repos-search/utils/make-owner-repo-Id';
import {OwnerRepo} from "../typedef";

export const RepoDetails = () => {
    let {url} = useRouteMatch();
    const params = useParams<OwnerRepo>();
    const dispatch = useDispatch();
    const repoInfo = useSelector(state => repoSelectors.selectById(state, makeOwnerRepoId(params)));

    useEffect(() => {
        dispatch(loadRepoInfo(params));
    });

    return (
        <Card>
            <CardHeader
                title={repoInfo?.name}
                subheader={repoInfo?.created_at}
            />
            <CardContent>
                {repoInfo?.description}
                <Box>License: {repoInfo?.license?.name}</Box>
                <Box>Language: {repoInfo?.language}</Box>
                <Box>Issues: <NavLink to={`${url}/issues`}>{repoInfo?.open_issues_count || 0}</NavLink></Box>
                <Box>Watchers: {repoInfo?.watchers_count}</Box>
                <Box>Forks: {repoInfo?.forks_count}</Box>
            </CardContent>
        </Card>
    )
}