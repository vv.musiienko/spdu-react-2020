import React, {FC} from 'react';
import {useHistory, useRouteMatch} from 'react-router-dom';
import {Avatar, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText, Icon, Box} from '@material-ui/core';
import StarIcon from '@material-ui/icons/Star';
import {RepoSearchResult} from '../typedef';

interface Props {
    repo: RepoSearchResult
}

const RepoItem: FC<Props> = ({repo}) => {
    let {path} = useRouteMatch();
    let history = useHistory();

    const openRepo = () => {
        history.replace(`${path}${repo.owner?.login}/${repo.name}`);
    }

    return (
        <ListItem button={true} onClick={openRepo}>
            <ListItemAvatar>
                <Avatar src={repo.owner?.avatar_url}/>
            </ListItemAvatar>
            <ListItemText>
                <div>{repo.full_name}</div>
                <div>{repo.description}</div>
            </ListItemText>
            <ListItemSecondaryAction>
                <Box>
                    <Icon color="primary">
                        <StarIcon/>
                    </Icon>
                    {repo.stargazers_count}
                </Box>
            </ListItemSecondaryAction>
        </ListItem>
    );
};

export default RepoItem;