import {useCallback, useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {useSelector, useDispatch} from 'react-redux';
import {clearData, loadRepos, repoSearchSelectors} from './services/repo-search-slice';
import RepoItem from './repo-item';
import {useQuery} from '../../hooks/use-query';
import SearchBar from 'material-ui-search-bar';
import {List, Typography} from '@material-ui/core';
import {Loader} from "../../../../components/loader/loader";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
}));


export const Repos = () => {
    const styles = useStyles();
    const dispatch = useDispatch();
    const browserQuery = useQuery();
    const [query, setQuery] = useState(browserQuery.query.get('search') || '');
    const repos = useSelector(repoSearchSelectors.selectAll);
    const loading = useSelector(state => state.reposSearch.loading);

    const loadData = () => {
        dispatch(loadRepos(query));
    };

    useEffect(() => {
        loadData()
        return () => {
            dispatch(clearData());
        }
    }, []);

    const onQueryChange = useCallback((value: string) => {
        setQuery(value);
    }, [])

    const search = () => {
        loadData();
        browserQuery.setQuery('search', query);
    }

    const clearQuery = () => {
        setQuery('');
        loadData();
    }

    return (
        <div className={styles.root}>
            <SearchBar
                value={query}
                onChange={onQueryChange}
                onCancelSearch={clearQuery}
                onRequestSearch={search}
            />
            {loading && <Loader/>}
            {
                !repos.length && !loading
                    ? <Typography align="center">No Data to display</Typography>
                    : (
                        <List dense={true}>
                            {repos.map(repo => <RepoItem repo={repo} key={repo.id}/>)}
                        </List>
                    )
            }
        </div>
    )
}

