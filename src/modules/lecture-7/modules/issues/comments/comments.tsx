import {useParams} from 'react-router-dom';
import {useEffect} from 'react';
import {Container} from '@material-ui/core';
import {CommentItem} from './comment-item';
import {CommentForm} from './comment-form';
import {useDispatch, useSelector} from 'react-redux';
import {deleteComment, loadComments} from './services/actions';
import {OwnerRepo} from "../../typedef";

interface RouterParams extends OwnerRepo {
    id: string
}

export const Comments = () => {
    const {id: idStr, owner, repo} = useParams<RouterParams>();
    const id = +idStr;
    const dispatch = useDispatch();
    const comments = useSelector(state => state.comments[owner]?.[repo]?.[id]);

    useEffect(() => {
        dispatch(loadComments(
            {
                issue_number: id,
                owner: owner,
                repo: repo
            }
        ));
    }, []);

    const removeComment = (commentId: number) => {
        dispatch(deleteComment({
            comment_id: commentId,
            owner: owner,
            repo: repo,
            issue_number: id
        }));
    }

    return (
        <Container>
            {
                comments?.map(comment => (
                    <CommentItem comment={comment} key={comment.id} removeComment={removeComment}/>
                ))
            }
            <CommentForm issueId={id}/>
        </Container>
    );
};
