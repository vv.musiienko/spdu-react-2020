import React, {ChangeEvent, FC, useCallback, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import {TextField} from '@material-ui/core';
import {useDispatch} from 'react-redux';
import {createComment} from './services/actions';

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        marginTop: 25
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

interface Props {
    issueId: number
}

export const CommentForm: FC<Props> = ({issueId}) => {
    const [comment, setComment] = useState('');
    const dispatch = useDispatch();

    const onCommentChange = useCallback((e: ChangeEvent<HTMLTextAreaElement>) => {
        setComment(e.target.value);
    }, [])

    const submitComment = () => {
        dispatch(createComment({
            owner: 'vlad-musienko',
            repo: 'ts-npm-module-template',
            issue_number: issueId,
            body: comment
        }));
        setComment('');
    }


    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent>
                <TextField
                    onChange={onCommentChange}
                    fullWidth
                    label="Leave a comment"
                    multiline
                    rows={4}
                    placeholder="Leave a comment"
                    value={comment}
                />
            </CardContent>
            <CardActions>
                <Button size="small" onClick={submitComment}>Submit</Button>
                <Button size="small">Close issue</Button>
            </CardActions>
        </Card>
    );
}