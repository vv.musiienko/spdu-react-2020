import {combineReducers} from 'redux';
import {tasks} from '../modules/board/services/reducers/tasks';

export const rootReducer = combineReducers({
    tasks
});
