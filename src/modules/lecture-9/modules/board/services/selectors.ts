import {AppState} from '../../../store';
import {TaskStatus} from '../constants';
import {Task} from '../typedef';
import {createSelector, Selector} from 'reselect';

const getTasks: Selector<AppState, Task[]> = (state) => state.tasks;

// Селектор выполнит пересчет при изменении результата getTasks или getStatus, т.е фильтрация будет перезапущена только при изменении данных
// https://github.com/reduxjs/reselect#q-how-do-i-create-a-selector-that-takes-an-argument
export const makeTasksByType = (status: TaskStatus) => createSelector(
    [getTasks],
    (tasks) => tasks.filter(task => task.status === status)
);

// Реселект не работает с параметрезированными селекторами, но мы создаем их через фукнцию, которая принимает статус и возвращает под конкретный селектор.
// Мы не можем просто передать в пропсы статус, т.к у нас 4 разных типа, и он просто будет постоянно пересчитывать.
export const getTodoTasks = makeTasksByType(TaskStatus.TODO);
export const getInProgressTasks = makeTasksByType(TaskStatus.IN_PROGRESS);
export const getReviewTasks = makeTasksByType(TaskStatus.REVIEW);
export const getDoneTasks = makeTasksByType(TaskStatus.DONE);
