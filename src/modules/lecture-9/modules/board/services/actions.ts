import {createAction} from '@reduxjs/toolkit';
import {AddTaskRequest, AddTaskResult, Task} from '../typedef';
import {apiCall} from '../../../api';
import {ApiDispatch} from '../../../store/store';

export const loadTasksSuccess = createAction<Task[]>('LOAD_TASKS_SUCCESS');
export const loadTasksFailure = createAction<void>('LOAD_TASKS_FAILURE');

export const loadTasks = () => (dispatch: ApiDispatch) => {
    apiCall<Task[]>(`board`)
        .then(result => dispatch(loadTasksSuccess(result)))
        .catch(() => {
            dispatch(loadTasksFailure())
        })
};


export const addTaskSuccess = createAction<Task>('ADD_TASK_SUCCESS');
export const addTaskFailure = createAction<void>('ADD_TASK_FAILURE');

export const addTask = (task: AddTaskRequest) => async (dispatch: ApiDispatch) => {

    try {
        const {id, rev} = await apiCall<AddTaskResult>('board', {
            method: 'POST',
            body: JSON.stringify(task)
        });
        dispatch(addTaskSuccess({
                ...task,
                _id: id,
                _rev: rev
            }
        ))
    } catch (e) {
        dispatch(addTaskFailure())
    }

};

export const removeTaskSuccess = createAction<string>('REMOVE_TASK_SUCCESS');

export const removeTask = (task: Task) => async (dispatch: ApiDispatch) => {
    await apiCall(`board/`, {
        method: 'DELETE',
        body: JSON.stringify(task)
    });

    dispatch(removeTaskSuccess(task._id));
};


export const updateTaskSuccess = createAction<Task>('UPDATE_TASK_SUCCESS');

export const updateTask = (task: Task) => async (dispatch: ApiDispatch) => {
    const {rev} = await apiCall('board', {
        method: 'PUT',
        body: JSON.stringify(task)
    });

    dispatch(updateTaskSuccess({...task, _rev: rev}));
};
