import {createReducer} from '@reduxjs/toolkit';
import {addTaskSuccess, loadTasksSuccess, removeTaskSuccess, updateTaskSuccess} from '../actions';
import {Task} from '../../typedef';

export const tasks = createReducer<Task[]>([], builder =>
    builder
        .addCase(loadTasksSuccess, (state, action) => {
            return action.payload;
        })
        .addCase(addTaskSuccess, (state, action) => {
            return [...state, action.payload];
        })
        .addCase(removeTaskSuccess, (state, action) => {
            return state.filter(task => task._id !== action.payload);
        })
        .addCase(updateTaskSuccess, (state, action) => {
            return [
                ...state.filter(task => task._id !== action.payload._id),
                action.payload
            ];
        })
);
