import React, {FC} from 'react';
import {TaskStatus} from './constants';
import clsx from 'clsx';

interface Props {
    status: TaskStatus,
    onChangeStatus: (status: TaskStatus) => void
}

const STATUSES = [
    {id: TaskStatus.TODO, caption: 'To Do'},
    {id: TaskStatus.IN_PROGRESS, caption: 'In-Progress'},
    {id: TaskStatus.REVIEW, caption: 'Review'},
    {id: TaskStatus.DONE, caption: 'Done'},
];
export const ChangeStatus: FC<Props> = ({status, onChangeStatus}) => (
    <div className="change-status dropdown show">
        {STATUSES.map(current => {
            return (
                <button
                    key={current.id}
                    type="button"
                    className={clsx('btn btn-light', {active: status === current.id})}
                    onClick={() => onChangeStatus(current.id)}>
                    {current.caption}
                </button>
            )
        })}
    </div>
);
