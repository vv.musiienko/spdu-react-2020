import React, {useEffect} from 'react';
import {BoardGroup} from './board-group';
import {TaskStatus} from './constants';
import {useDispatch, useSelector} from 'react-redux';
import {loadTasks} from './services/actions';
import {getDoneTasks, getInProgressTasks, getReviewTasks, getTodoTasks} from './services/selectors';
import './board.css';


export const Board = () => {
    const dispatch = useDispatch();
    console.log(dispatch);

    const todo = useSelector(getTodoTasks);
    const inProgress = useSelector(getInProgressTasks);
    const review = useSelector(getReviewTasks);
    const done = useSelector(getDoneTasks);

    useEffect(() => {
        dispatch(loadTasks())
    }, []);

    return (
        <>
            <div className="container-fluid pt-3">
                <div className="row flex-row flex-sm-nowrap py-3">
                    <BoardGroup status={TaskStatus.TODO} title="To Do" tasks={todo}/>
                    <BoardGroup status={TaskStatus.IN_PROGRESS} title="In-progress" tasks={inProgress}/>
                    <BoardGroup status={TaskStatus.REVIEW} title="Review" tasks={review}/>
                    <BoardGroup status={TaskStatus.DONE} title="Done" tasks={done}/>
                </div>
            </div>
        </>
    )
};
