import React, {FC, useCallback, useState} from 'react';
import {ChangeStatus} from './change-status';
import {Task} from './typedef';
import {useDispatch} from 'react-redux';
import {removeTask as removeTaskAction, updateTask as updateTaskAction} from './services/actions';

interface Props {
    task: Task
}

export const BoardCard: FC<Props> = ({task}) => {
    const [edit, setEdit] = useState(false);
    const [title, setTitle] = useState(task.title);
    const [description, setDescription] = useState(task.description);
    const [status, setStatus] = useState(task.status);
    const dispatch = useDispatch();

    const updateTask = useCallback(async () => {
        await dispatch(updateTaskAction({
            ...task,
            description,
            title,
            status
        }));
        setEdit(false);
    }, [description, title, status, task]);

    const toggleEdit = useCallback(() => {
        setEdit(!edit);
    }, [edit]);


    const onChangeStatus = useCallback(async (status) => {
        await dispatch(updateTaskAction({
            ...task,
            description,
            title,
            status
        }));
        setStatus(status);
    }, []);

    const removeTask = useCallback(() => {
        dispatch(removeTaskAction(task));
    }, []);

    return (
        <>
            <div className="card draggable shadow-sm">
                <div className="card-body p-2">
                    <div className="card-title">
                        <i className="fas fa-trash-alt float-right" onClick={removeTask}/>
                        {edit
                            ? <i className="fas fa-check float-right" onClick={updateTask}/>
                            : <i className="far fa-edit float-right" onClick={toggleEdit}/>
                        }
                        {edit
                            ? <input className="form-control col-11" type="text" value={title}
                                     onChange={e => setTitle(e.currentTarget.value)}/>
                            : <p>{title}</p>
                        }
                    </div>
                    {edit
                        ? <textarea
                            className="form-control"
                            value={description}
                            onChange={e => setDescription(e.currentTarget.value)}
                        />
                        : <p>{description}</p>
                    }
                    <ChangeStatus status={status} onChangeStatus={onChangeStatus}/>
                </div>
            </div>
            <div> &nbsp; </div>
        </>
    );
};
