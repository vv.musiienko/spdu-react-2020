import {TaskStatus} from './constants';


export type Task = {
    _id: string,
    _rev: string,
    title: string,
    description: string,
    status: TaskStatus
}


export type AddTaskResult = {
    id: string,
    rev: string
}

export type AddTaskRequest = Omit<Task, '_id' | '_rev'>;
