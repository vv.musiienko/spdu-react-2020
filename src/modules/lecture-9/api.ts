export async function apiCall<R>(url: string, options?: RequestInit): Promise<R> {
    return fetch(`http://localhost:3005/${url}`,
        {
            headers: {'Content-Type': 'application/json'},
            ...options
        })
        .then(res => res.json())
        .then(res => res)
}
