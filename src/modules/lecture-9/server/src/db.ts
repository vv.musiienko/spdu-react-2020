import * as PouchDB from 'pouchdb-node';
import * as PouchDbFind from 'pouchdb-find';

PouchDB.plugin(PouchDbFind);
export const db = new PouchDB('kanban-board');

