import faker from 'faker';
import { User } from './typedef';

const generateUser = (): User => {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();

    return {
        name: `${firstName} ${lastName}`,
        email: faker.internet.email(firstName, lastName),
        phone: faker.phone.phoneNumber(),
        photoUrl: faker.image.image(),
        gitlab: faker.internet.url(),
        facebook: faker.internet.url(),
        linkedIn: faker.internet.url(),
    }
}


export const students: User[] = new Array(250)
    .fill(null)
    .map(() => generateUser())