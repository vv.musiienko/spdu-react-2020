import React from 'react';
import {Route, Switch, useRouteMatch} from 'react-router-dom';
import { ClassExample } from './examples/class-example';
import {FunctionalExample} from './examples/functional-example';
import { Parent } from './examples/parent-child-example';
import { PropsExample } from './examples/props-example.';
import { StateExample } from './examples/state-example';
import {InputEventHandlingExample} from './examples/input-event-handling-example';
import {PureComponentExample} from './examples/pure-component-example';



export const Lecture2Examples = () => {
    let { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/class-example`} component={() => <ClassExample name="Test"/> }/>
            <Route path={`${path}/functional-example`} component={FunctionalExample}/>
            <Route path={`${path}/props-example`} component={() => <PropsExample data={['a', 'b']}/>}/>
            <Route path={`${path}/state-example`} component={() => <StateExample caption="Click me!" type="PRIMARY"/>}/>
            <Route path={`${path}/pure-component-example`} component={PureComponentExample}/>
            <Route path={`${path}/parent-child-example`} component={Parent}/>
            <Route path={`${path}/input-event-handling-example`} component={InputEventHandlingExample}/>
        </Switch>
    )
};

