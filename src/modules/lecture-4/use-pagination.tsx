import React, {useEffect, useState} from 'react';
import {Pagination} from '@material-ui/lab';


export function usePagination<Type>(data: Array<Type>, pageSize: number) {
    const [page, setPage] = useState(1);
    const [pageData, setPageData] = useState<Type[]>([]);
    const [totalPages, setTotalPages] = useState(0);

    const total = data.length;
    const offsetStart = (page - 1) * pageSize;
    const offsetEnd = page * pageSize;

    useEffect(() => setTotalPages(Math.ceil(total / pageSize)), [data]);

    useEffect(() => setPageData(data.slice(offsetStart, offsetEnd)), [page]);

    const pagination = <Pagination color="secondary" count={totalPages} onChange={(e, page) => setPage(page)}/>;

    return {
        page,
        setPage,
        pageData,
        totalPages,
        pagination
    }
}

