import {FC, ReactNode, useState} from 'react';
import {useShown} from './useShown';

type Props = {
    anchor: ReactNode,
    children: ReactNode
}

export const ToggleText: FC<Props> = ({anchor, children}) => {
   const {shown, toggleShown} = useShown();

    return (
        <div>
            <div onClick={toggleShown}>{anchor}</div>
            {shown ? children : null}
        </div>
    )
}