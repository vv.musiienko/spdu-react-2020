import {FC, ReactNode} from 'react';
import {useShown} from './useShown';

interface Props {
    anchor: ReactNode,
    children: ReactNode
}

export const Tooltip: FC<Props> = ({anchor, children}) => {
    const {shown, show, hide} = useShown();

    return (
        <div onMouseEnter={show} onMouseLeave={hide}>
            {anchor}
            {shown ? <div style={{border: '1px solid black', background: '#FFF'}}>{children}</div> : null}
        </div>

    )
}