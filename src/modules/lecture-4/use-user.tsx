import {useEffect, useState} from "react";

export type User = {
    name: string,
    avatar: string
};

export const useUser = (userId: string) => {
  const [user, setUser] = useState<User>();

    useEffect(() => {
            fetch(`https://api.github.com/users/${userId}`)
                .then(result => result.json())
                .then((githubUser) => {
                    const user = {
                        name: githubUser.login,
                        avatar: githubUser.avatar_url
                    };
                    setUser(user)
                });
        },
        [userId]
    );

  return {user};
};
