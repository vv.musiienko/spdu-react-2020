import React, {useMemo, useState} from 'react';


export const HeavyCalc = () => {
    const [multiplier, setMultiplier] = useState<number>(0);
    const [count, setCount] = useState<number>(0);
    const [data, setData] = useState<number>(0)

    useMemo(() => {
        console.time('total');
        const data = new Array(2e7).fill(null).map((value, index) => index);
        const result = data.reduce((memo, current) => memo + current * multiplier, 0);

        setData(result);
        console.timeEnd('total');
    }, [multiplier]);



    console.log('render');
    return (
        <div>
            <button onClick={() => setMultiplier(1)}>1x</button>
            <button onClick={() => setMultiplier(2)}>2x</button>
            <button onClick={() => setMultiplier(3)}>3x</button>
            <button onClick={() => setCount(count + 1)}>Increment</button>
            <div>Total: {data}</div>
            <div>Count: {count}</div>
        </div>
    )
};

HeavyCalc.routeName = 'heavy-calc-example';

