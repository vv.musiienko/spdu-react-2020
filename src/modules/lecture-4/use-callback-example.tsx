import React, {FC, PureComponent, useCallback, useState} from 'react';

type Props = {};


export const UseCallbackExample: FC<Props> = () => {
    const [count, setCount] = useState(0);
    const [items, setItems] = useState([
        {
            id: 'BAN',
            caption: 'Banana'
        },
        {
            id: 'APPL',
            caption: 'Apple'
        },
        {
            id: 'ORG',
            caption: 'Orange'
        }
    ]);

    const onRemove = useCallback((id: string) => {
        setItems(items.filter(current => current.id !== id));
    }, [items]);

    return (
        <div>
            {count}
            <button onClick={() => setCount(count + 1)}>Increment</button>
            {items.map(item => {
                return <Item key={item.id} id={item.id} caption={item.caption} onRemove={onRemove}/>;
            })}
        </div>
    )
};


type ItemProps = {
    onRemove: (id: string) => void,
    caption: string,
    id: string
};

class Item extends PureComponent<ItemProps> {
    render() {
        console.log('rerender');
        return (
            <button onClick={() => this.props.onRemove(this.props.id)}>{this.props.caption}</button>
        );
    }
}
