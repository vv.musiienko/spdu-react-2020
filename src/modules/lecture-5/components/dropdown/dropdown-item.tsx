import React, {FC, ReactNode} from 'react';

type Props = {
    handler: () => void,
    children: ReactNode
};

export const Item: FC<Props> = props => {
    return (
        <a className="dropdown-item" href="#" onClick={props.handler}>{props.children}</a>
    )
};
