import React, {FC} from 'react';
import clsx from 'clsx';

type Props = {
    size: number,
    src: string
    rounded?: boolean,
};

export const Avatar: FC<Props> = ({size, rounded, src}) => {
    const imgClasses = clsx('avatar__image', {'avatar__image_rounded': rounded});
    return (
        <div className="avatar">
            <img className={imgClasses} src={src} style={{width: `${size}px`}} alt="Avatar"/>
        </div>
    )
};
