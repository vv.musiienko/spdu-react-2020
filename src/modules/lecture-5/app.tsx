import React, {lazy, Suspense} from 'react';
import {Switch, Route, useRouteMatch} from 'react-router-dom';
import {Navigation} from './modules/navigation/navigation';
import {UsersMain} from './modules/users';
import {About} from './modules/about';
import {Home} from './modules/home';
import {Loader} from '../../components/loader/loader';


const News = lazy(() => import('./modules/news'));

export const Lecture5Examples = () => {
    let {path} = useRouteMatch();
    return (
        <>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
                  crossOrigin="anonymous"/>
            <Navigation/>
            <Suspense fallback={<Loader/>}>
                <Switch>
                    <Route path={`${path}/home`} component={Home}/>
                    <Route path={`${path}/about`} component={About}/>
                    <Route path={`${path}/news`} component={News}/>
                    <Route path={`${path}/users`} component={UsersMain}/>
                </Switch>
            </Suspense>
        </>
    );
};
