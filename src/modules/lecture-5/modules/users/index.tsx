import React from "react";
import {Route, Switch, useRouteMatch} from "react-router-dom";
import { Users } from "./users";
import {UserDetails} from "./user-details";
import {UserRepos} from "./user-repos";

export const UsersMain = () => {
    const {path} = useRouteMatch();


    return (
        <Switch>
            <Route path={path} component={Users} exact/>
            <Route path={`${path}/:userId`} component={UserDetails} exact/>
            <Route path={`${path}/:userId/repos`} component={UserRepos} />
        </Switch>
    )
}
