import React, {FC, useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {octokit} from '../../../../octokit';
import {GetResponseDataTypeFromEndpointMethod} from "@octokit/types";

type Props = {};

interface RouterParams {
    userId: string
}

export const UserRepos: FC<Props> = () => {
    const {userId} = useParams<RouterParams>();
    const [repos, setRepos] = useState<GetResponseDataTypeFromEndpointMethod<typeof octokit.repos.listForUser>>([]);
    useEffect(() => {
        octokit.repos.listForUser({username: userId})
            .then((response) => {
                setRepos(response.data);
            })
    }, [userId]);

    return (
        <>
            Repos
            {
                repos.map(repo => (
                    <div key={repo.id}>
                        <div>{repo.name} - {repo.language}</div>
                    </div>
                ))
            }
        </>
    )
};
