import React, {FC, useEffect, useState} from 'react';
import {octokit} from '../../../../octokit';
import {Avatar} from '../../components/avatar/avatar';
import {Link, useParams} from 'react-router-dom';
import {GetResponseDataTypeFromEndpointMethod} from '@octokit/types';

type RouterParams = {
    userId: string
}

export const UserDetails: FC = () => {
    const {userId} = useParams<RouterParams>();

    const [userInfo, setUserInfo] = useState<GetResponseDataTypeFromEndpointMethod<typeof octokit.users.getByUsername>>();

    useEffect(() => {
        if (!userId) {
            return;
        }
        octokit.users.getByUsername({username: userId})
            .then(response => {
                setUserInfo(response.data);
            });
    }, [userId]);

    if (userInfo === undefined) {
        return null;
    }

    return (
        <>
            <h3>{userInfo.name}</h3>
            <div>
                <Avatar size={128} src={userInfo.avatar_url}/>
            </div>
            <dl>
                <dt>Company</dt>
                <dd>{userInfo.company}</dd>
                <dt>Biography</dt>
                <dd>{userInfo.bio}</dd>
                <dt>Followers</dt>
                <dd>{userInfo.followers}</dd>
                <dt>Location</dt>
                <dd>{userInfo.location}</dd>
                <dt>Blog</dt>
                <dd>{userInfo.blog}</dd>
            </dl>
            <Link to={`/lecture-5/users/${userId}/repos`}>Show user repos</Link>
        </>
    )
};
