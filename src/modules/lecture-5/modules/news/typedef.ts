export type NewsItem = {
    author: string,
    content: string,
    description: string,
    publishedAt: string,
    title: string,
    urlToImage: string
}
