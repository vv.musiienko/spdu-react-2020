import React, {FC} from "react";
// DON'T WRITE CODE IN A SINGLE FILE, IT'S JUST AN EXAMPLE

type User = {
    name: string,
    avatar: string
};

const App: FC<{}> = () => {
    const user = {
        name: 'No Name User',
        avatar: 'https://picurl.com/noname'
    };

    return (
        <>
            <Navigation user={user}/>
        </>
    );
};


type NavigationProps = {
    user: User
}

const Navigation = (props: NavigationProps) => {
    return (
        <>
            Navigation here
            <UserProfile user={props.user}/>
        </>
    )

};


type UserProfileProps = {
    user: User
};

const UserProfile: FC<UserProfileProps> = (props) => (
    <>
        {props.user.name}
        <img src={props.user.avatar} alt={`${props.user.name} Photo`}/>
    </>
);
