export type User = {
    name: string,
    avatar: string
};

export type Dictionary = {
    navigation: {
        home: string,
        about: string,
        contacts: string,
        news: string
    }
}

export type Lang = 'en' | 'ua';
