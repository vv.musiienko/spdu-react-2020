import React, {FC, ReactNode, useState} from 'react';
import clsx from 'clsx';

type Props = {
    caption: string,
    children: ReactNode
};

export const Dropdown: FC<Props> = props => {
    const [shown, setShown] = useState(false);
    const dropdownMenuClasses = clsx('dropdown-menu', {'show': shown});

    return (
        <div className="dropdown show">
            <button className="btn dropdown-toggle" type="button" onClick={() => setShown(!shown)}>
                {props.caption}
            </button>
            <div className={dropdownMenuClasses}>
                {props.children}
            </div>
        </div>
    )
};
