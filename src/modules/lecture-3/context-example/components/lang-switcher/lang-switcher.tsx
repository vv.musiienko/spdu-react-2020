import React, {FC} from 'react';
import {Lang} from '../../typedef';
import {Dropdown, Item as DropdownItem} from '../dropdown';

type Props = {
    lang: Lang,
    changeLang: (lang: Lang) => void
};

export const LangSwitcher: FC<Props> = props => {
    const createHandler = (lang: Lang) => () => {
        props.changeLang(lang)
    };

    return (
        <>
            <Dropdown caption={props.lang}>
                <DropdownItem handler={createHandler('en')}>en</DropdownItem>
                <DropdownItem handler={createHandler('ua')}>ua</DropdownItem>
            </Dropdown>
        </>
    )
};
