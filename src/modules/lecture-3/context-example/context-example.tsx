import React, {FC, useEffect, useState} from 'react';
import {Navigation} from './modules/navigation/navigation';
import {AppContext} from './app.context';
import {Dictionary, Lang, User} from './typedef';

export const ContextExample: FC<{}> = () => {
    const [user, setUser] = useState<User>();
    const [lang, setLang] = useState<Lang>('en');
    const [dictionary, setDictionary] = useState<Dictionary>();

    useEffect(() => {
        fetch(`/i18n/${lang}.json`)
            .then(result => result.json())
            .then((data) => {
                setDictionary(data);
            });
    }, [lang]);

    useEffect(() => {
            fetch('https://api.github.com/users/vlad-musienko')
                .then(result => result.json())
                .then((githubUser) => {
                    const user = {
                        name: githubUser.login,
                        avatar: githubUser.avatar_url
                    };
                    setUser(user)
                });
        },
        []
    );

    if (!dictionary) {
        return null;
    }

    const contextValue = {
        user,
        dictionary,
        changeLang: (lang: Lang) => setLang(lang),
        lang
    };

    return (
        <AppContext.Provider value={contextValue}>
            <div className="container">
                <Navigation/>

                <pre>
                    {JSON.stringify(user, null, 4)}
                </pre>
            </div>
        </AppContext.Provider>
    )
};
