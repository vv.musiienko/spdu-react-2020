import {createContext} from 'react';
import {Dictionary, Lang, User} from './typedef';

export type Context = {
    user?: User,
    dictionary: Dictionary,
    changeLang: (lang: Lang) => void,
    lang: Lang
}

export const AppContext = createContext<Context | null>(null);
