import {Route, Switch, useRouteMatch} from 'react-router-dom';
import {ContextExample} from './context-example/context-example';
import {ContextExampleClass} from './context-example/app.class';
import {RefExampleFn, RefsExample} from './refs-example';

export const Lecture3Examples = () => {
    let { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/context-example`} component={ContextExample}/>
            <Route path={`${path}/context-example-class`} component={ContextExampleClass}/>
            <Route path={`${path}/refs-example`} component={RefsExample}/>
            <Route path={`${path}/refs-hook`} component={RefExampleFn}/>
        </Switch>
    )
};