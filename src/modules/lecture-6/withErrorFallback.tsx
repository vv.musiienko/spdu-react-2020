import React, {Component, ComponentType} from 'react';

type State = {
    hasError: boolean
};

export function withErrorFallback<T>(WrappedComponent: ComponentType<T>) {
    return class extends Component<{}, State> {
        state: State = {
            hasError: false
        };

        static getDerivedStateFromError() {
            return {hasError: true};
        }

        render() {
            if (this.state.hasError) {
                return <div>Something went wrong.</div>;
            }

            return <WrappedComponent {...this.props as any}/>;
        }
    }
}
