import React from 'react';
import img1 from './assets/panorama+900+x+300+100ppi-1.jpg';
import img2 from './assets/panorama+900+x+300+100ppi-2.jpg';
import img4 from './assets/panorama+900+x+300+100ppi-4.jpg';
import img3 from './assets/panorama+900+x+300+100ppi-6.jpg';
import img5 from './assets/panorama+900+x+300+100ppi-7.jpg';
import img6 from './assets/panorama+900+x+300+100ppi-12.jpg';
import {Carousel, CarouselItem} from '../components/carousel';

export const Carousels = () => {
    return (
        <div>
            <Carousel width={900} height={300}>
                <CarouselItem>
                    <img src={img1} alt="slide1"/>
                </CarouselItem>
                <CarouselItem>
                    <img src={img2} alt="slide2"/>
                </CarouselItem>
                <CarouselItem>
                    <img src={img3} alt="slide3"/>
                </CarouselItem>
                <CarouselItem>
                    <img src={img4} alt="slide4"/>
                </CarouselItem>
                <CarouselItem>
                    <img src={img5} alt="slide5"/>
                </CarouselItem>
                <CarouselItem>
                    <img src={img6} alt="slide6"/>
                </CarouselItem>
                <CarouselItem>
                    <iframe title="yt-patterns" width="930" height="300" src="https://www.youtube.com/embed/8-bYp0IXcZM" frameBorder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen/>
                </CarouselItem>
                <CarouselItem>
                    <iframe title="self" frameBorder={0} width={930} height={300} src="http://localhost:3000/lecture-5"/>
                </CarouselItem>
            </Carousel>
        </div>
    );
};
