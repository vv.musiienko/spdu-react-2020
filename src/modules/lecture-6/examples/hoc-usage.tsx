import React, {FC} from 'react';
import {withPagination, WithPaginationProps} from '../withPagination';

interface Props extends WithPaginationProps<Item> {
}

type Item = {
    id: number,
    caption: string
}

export const HocUsage: FC<Props> = ({pageData}) => {
    return (
        <>{pageData.map(item => <div key={item.id}>{item.caption}</div>)}</>
    )
};


export default withPagination<Item>(HocUsage);
