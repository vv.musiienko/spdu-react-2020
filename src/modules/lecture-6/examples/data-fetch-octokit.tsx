import {DataFetchSection} from '../data-fetch-section';
import {Loader} from '../../../components/loader/loader';

type Data = {
    name: string
}

export const Orgs = () => {
    return (
        <DataFetchSection<Data> url="https://api.github.com/orgs/facebook/repos">
            {({data, loading, refresh}) => (
                <>
                    {loading && <Loader/>}
                    <button onClick={refresh}>refresh</button>
                    {data && <pre>{JSON.stringify(data, null, 4)}</pre>}
                </>
            )}
        </DataFetchSection>
    )
}
