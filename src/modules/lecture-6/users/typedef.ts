import {components} from '@octokit/openapi-types/generated/types';

export type Contributor = components['schemas']['contributor'];