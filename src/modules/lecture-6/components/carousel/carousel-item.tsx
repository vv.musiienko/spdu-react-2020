import React, {FC, ReactNode} from 'react';

interface Props {
    children: ReactNode
}

export const CarouselItem: FC<Props> = ({children}) => {
    return (
        <div className="carousel__item">
            {children}
        </div>
    );
};
