import {FC, ReactElement, useState} from 'react';
import {CarouselItem} from './carousel-item';
import './carousel.scss';

interface Props {
    children: ReactElement<typeof CarouselItem>[],
    width?: number | string,
    height?: number | string
}

export const Carousel: FC<Props> = ({width = '100%', height = '100%', children}) => {
    const [activeSlide, setActiveSlide] = useState(0);
    const nextDisabled = activeSlide === children.length - 1;
    const prevDisabled = activeSlide === 0;

    const next = () => {
        setActiveSlide(activeSlide => activeSlide + 1);
    }

    const prev = () => {
        setActiveSlide(activeSlide => activeSlide - 1);
    }
    return (
        <div className="carousel" style={{width, height}}>
            <div className="carousel__viewport">
                <div key={activeSlide}>{children[activeSlide]}</div>
            </div>
            <button className="carousel__controls carousel__controls_left" onClick={prev} disabled={prevDisabled}>
                <i className="carousel__arrow fa fa-arrow-left"/>
            </button>
            <button className="carousel__controls carousel__controls_right" onClick={next} disabled={nextDisabled}>
                <i className="carousel__arrow fa fa-arrow-right"/>
            </button>
        </div>
    );
};
