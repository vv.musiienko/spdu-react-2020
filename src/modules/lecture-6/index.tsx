import {Redirect, Route, Switch, useRouteMatch} from 'react-router-dom';
import {Carousels} from './examples/carousels';
import HocUsage from './examples/hoc-usage';
import WithErrorTest from './examples/with-error-example';
import {UsersList} from './users/users-list';
import {UsersGrid} from './users/users-grid';
import UsersView from './users/users-custom';
import {UseLoggerExample} from './examples/use-logger-example';
import {UseFilterExample} from './examples/use-filter-example';
import {Orgs} from './examples/data-fetch-octokit';

const exampleDataSet: Array<{ id: number, caption: string }> = new Array(100)
    .fill(null)
    .map((_, idx) => {
        return {
            id: idx,
            caption: `Item: ${idx}`
        }
    });

export const Lecture6Examples = () => {
    let {path} = useRouteMatch();
    return (
        <>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
                  crossOrigin="anonymous"/>
            <Switch>
                <Route path={`${path}/carousel`} component={Carousels}/>
                <Route path={`${path}/use-logger`} component={UseLoggerExample}/>
                <Route path={`${path}/use-filter`} component={UseFilterExample}/>
                <Route path={`${path}/hoc-usage`}>
                    <HocUsage data={exampleDataSet}/>
                </Route>
                <Route path={`${path}/with-error-fallback`} component={WithErrorTest}/>
                <Route path={`${path}/users/list`} component={UsersList}/>
                <Route path={`${path}/users/grid`} component={UsersGrid}/>
                <Route path={`${path}/users/grid-paginated`} component={() => <UsersView/>}/>
                <Route path={`${path}/df-octokit`} component={Orgs}/>
                <Redirect to={`${path}/carousel`}/>
            </Switch>
        </>
    );
}
