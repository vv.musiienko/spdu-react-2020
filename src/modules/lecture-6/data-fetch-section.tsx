import {ReactElement, useEffect, useState} from 'react';

type RefreshFn = () => void;

type CallbackParams<T> = { data: T | null, loading: boolean, refresh: RefreshFn }

interface Props<T> {
    children: (params: CallbackParams<T>) => ReactElement,
    url: string
}

export function DataFetchSection<T>(props: Props<T>) {
    const [data, setData] = useState<T | null>(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        fetchData();
    }, []);


    // const fetchData = () => {
    //     setData(null);
    //     setLoading(true);
    //     fetch(props.url)
    //         .then(response => response.json())
    //         .then(response => setData(response))
    //         .then(() => setLoading(false));
    // }

    const fetchData = async () => {
        setLoading(true);
        const response = await fetch(props.url);
        const data = await response.json();
        setData(data);
        setLoading(false);
    };

    return props.children({
        data,
        loading,
        refresh: fetchData
    });

}

